from frappe import _

def get_data():
	return [
		{	"module_name": "xlevel_petro",
			"label": _("Sales"),
			"icon": "icon-star",
			"items": [
				{	"type": "doctype",
					"name": "Sales Entry",
					"label": "Sales Entry"
				}
			]
		}
]