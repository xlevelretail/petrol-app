# -*- coding: utf-8 -*-
# Copyright (c) 2018, hello@openetech.com and contributors
# For license information, please see license.txt
from __future__ import unicode_literals
import frappe
from frappe.model.document import Document
from frappe.utils import nowdate
from erpnext.accounts.doctype.payment_entry.payment_entry import get_payment_entry, get_company_defaults
from erpnext.accounts.party import get_party_account
from erpnext.accounts.utils import get_account_currency
from frappe import _

class SalesEntry(Document):
	def make_customer(self):
		customer_doc = frappe.new_doc('Customer')
		customer_doc.naming_series = "CUST-"
		customer_doc.customer_name = self.customer_name
		customer_doc.customer_type = 'Individual'
		customer_doc.territory = frappe.db.get_single_value('Selling Settings','territory') or _('All Territories')
		customer_doc.customer_group = frappe.db.get_single_value('Selling Settings','customer_group') or frappe.db.get_value('Customer Group', {'is_group': 0}, 'name')
		customer_doc.flags.ignore_mandatory = True
		customer_doc.insert(ignore_permissions=True)

	def make_sales_invoice(self):
		si_doc = frappe.new_doc('Sales Invoice')
		si_doc.company = self.company
		si_doc.currency = frappe.db.get_value("Company", self.company, "default_currency")
		si_doc.customer = self.customer_name
		si_doc.posting_date = nowdate()
		si_doc.due_date = nowdate()
		si_doc.update_stock = 1
		si_doc.x_sales_entry = self.name
		for d in self.sales_item:
			si_doc.append("items", {
				"item_code": d.item_code,
				"qty": (d.available_qty - d.qty),
				"rate": d.rate,
				"cost_center": self.cost_center,
				"warehouse": self.warehouse
			})
		si_doc.set_missing_values()
		si_doc.flags.ignore_mandatory = True
		si_doc.insert(ignore_permissions=True)
		si_doc.submit()

		if frappe.db.exists("Sales Invoice",{"name": si_doc.name}):
			apply_adjustment = False
			adj_to_be_applied = False
			company = si_doc.company
			frappe.flags.ignore_account_permission = True
			company_details = get_company_defaults(company)
			party_account = si_doc.debit_to
			party_account_currency = si_doc.get("party_account_currency") or get_account_currency(party_account)
			bank_amount = si_doc.grand_total

			if party_account_currency == si_doc.company_currency and party_account_currency != si_doc.currency:
				party_amount = si_doc.base_grand_total
			else:
				party_amount = si_doc.grand_total
			
			if self.difference_amount >= 0:
				diff_amount = self.difference_amount
			else:
				apply_adjustment = True
				adj_to_be_applied = True
				diff_amount = -1 * self.difference_amount
			
			for d in self.payment_entry:
				mode_of_payment = d.mode_of_payment
				if apply_adjustment:
					if d.amount > diff_amount:
						amount = d.amount - diff_amount
						apply_adjustment = False
					else:
						amount = d.amount
				else:
					amount = d.amount
	
				account = frappe.db.sql("""select default_account 
						from `tabMode of Payment Account`
						where company = %s and parent = %s""",(company,mode_of_payment))
				if account:
					payment_account = account[0][0]
				else:
					frappe.throw(_("Please set up account in mode of payment"))

				payment_entry = get_payment_entry("Sales Invoice", si_doc.name,
					party_amount=party_amount, bank_account=payment_account, bank_amount=amount)

				payment_entry.update({
				"reference_no": self.name,
				"reference_date": nowdate(),
				"remarks": "Payment Entry against {0} {1} via Sales Enty {2}".format("Sales Invoice",
					si_doc.name, self.name),
					"mode_of_payment":mode_of_payment,"base_paid_amount":amount,"base_received_amount":amount,
					"base_total_allocated_amount":amount,"paid_amount":amount,"received_amount":amount,
					"total_allocated_amount":amount,"allocate_payment_amount":0,"difference_amount": 0
					})

				payment_entry.set("references",[])
				payment_entry.append("references",
					{"__islocal": 1,"allocated_amount": amount,"docstatus": 0,"doctype": "Payment Entry Reference",
					"due_date": nowdate(),"exchange_rate": 1.0,"idx": 1,"outstanding_amount": si_doc.outstanding_amount,
					"parentfield": "references","parenttype": "Payment Entry","reference_doctype": "Sales Invoice",
					"reference_name": si_doc.name,"total_amount": si_doc.outstanding_amount}
					)
				payment_entry.insert(ignore_permissions=True)
				payment_entry.submit()
			if apply_adjustment:
				frappe.throw(_("Difference Amount cannot be adjusted as Payment Amount per Mode of Payment is less than amount to be adjusted"))
			if diff_amount > 0:
				jv = frappe.new_doc("Journal Entry")
				jv.posting_date = nowdate()
				jv.company = si_doc.company
				jv.user_remark = "Write Off Journal Entry against {0} {1} via Sales Entry {2}".format("Sales Invoice",
				si_doc.name, self.name)
				if adj_to_be_applied:
					jv.set("accounts", [
						{"party_type":"Customer","party": si_doc.customer,"account":si_doc.debit_to,
						"cost_center": self.cost_center,"credit_in_account_currency": diff_amount,
						"debit_in_account_currency": 0,"exchange_rate": 1,"is_advance": "Yes"
						},
						{"account": company_details.write_off_account,"cost_center": self.cost_center,
						"debit_in_account_currency": diff_amount,"credit_in_account_currency": 0,"exchange_rate": 1
						}])
					adj_to_be_applied = False
				else:
					jv.set("accounts", [
						{"party_type":"Customer","party": si_doc.customer,"account":si_doc.debit_to,
						"reference_type":"Sales Invoice","reference_name":si_doc.name,"cost_center": self.cost_center,
						"credit_in_account_currency": diff_amount,"debit_in_account_currency": 0,"exchange_rate": 1
						},
						{"account": company_details.write_off_account,"cost_center": self.cost_center,
						"debit_in_account_currency": diff_amount,"credit_in_account_currency": 0,"exchange_rate": 1
						}])
				jv.insert(ignore_permissions=True)
				jv.submit()

	def validate(self):
		si_total = 0
		pay_total = 0
		for si in self.sales_item:
			si_total = si_total + float(si.amount)
			if si.qty > si.available_qty:
				frappe.throw(_("Ending Quantity cannot be greater than Available Quantity"))
			if (si.qty - si.available_qty) == 0:
				frappe.throw(_("Available Quantity and ending quantity cannot be the same"))
		for py in self.payment_entry:
			pay_total = pay_total + float(py.amount)
		if si_total == 0 or pay_total == 0:
			frappe.throw(_("Sales and Payment amounts should be greater than 0"))
		if (round(si_total,2) - round(pay_total,2))!= 0:
			frappe.msgprint("Sales & Payment Total do not match. HQ clarification would be needed")
		self.difference_amount = round(si_total,2) - round(pay_total,2)

		for d in self.payment_entry:
			if not d.mode_of_payment:
				frappe.throw(_("Mode of Payment is required"))
			if not d.amount:
				frappe.throw(_("Payment Amount is required"))
			mode_of_payment = d.mode_of_payment
			account = frappe.db.sql("""select default_account 
						from `tabMode of Payment Account`
						where company = %s and parent = %s""",(self.company,mode_of_payment))
			if account:
				pass
			else:
				frappe.throw(_("Please set up account in mode of payment {0}").format(mode_of_payment))

	def on_submit(self):
		if not frappe.db.exists("Customer",{"customer_name": self.customer_name}):
			self.make_customer()
		self.posting_date = frappe.utils.nowdate()
		self.posting_time = frappe.utils.nowtime()
		self.make_sales_invoice()
	
@frappe.whitelist()
def get_warehouse_qty(item_code,warehouse):
	available_qty = 0
	bin = frappe.db.sql("""select ifnull(actual_qty,0) from `tabBin`
					where item_code = %s and warehouse = %s""",(item_code,warehouse))
	if bin:
		available_qty = bin[0][0]
	rate = 0
	user = frappe.session.user
	price_list_rate = frappe.db.sql("""select ifnull(a.price_list_rate,0)
						from `tabItem Price` a
						where a.item_code = %s and a.selling = 1 
						and a.price_list in (select c.for_value 
										from `tabUser Permission` c
										where c.allow = 'Price List'
										and c.user = %s)""", (item_code, user))

	if price_list_rate:
		rate = price_list_rate[0][0]
	if not rate or rate == 0:
		frappe.throw(_("No rate set up for the item. Also check user permission for the Price List"))
	return {"available_qty":available_qty,"rate":rate}	