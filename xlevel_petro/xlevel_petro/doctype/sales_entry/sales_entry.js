// Copyright (c) 2018, hello@openetech.com and contributors
// For license information, please see license.txt

frappe.provide("xlevel_petro.sales_entry");
frappe.ui.form.on("Sales Entry Item", {
	item_code: (frm, cdt, cdn) => {
		xlevel_petro.sales_entry.update_item_rate_qty(frm, cdt, cdn);
		xlevel_petro.sales_entry.update_amount(frm, cdt, cdn);
	},
	qty: (frm, cdt, cdn) => {
		xlevel_petro.sales_entry.update_amount(frm, cdt, cdn);
	}
});

xlevel_petro.sales_entry.update_item_rate_qty = function(frm, cdt, cdn) {
	let entry_list = ['sales_item'];
	var warehouse = frm.doc.warehouse;
	entry_list.forEach((entry) => {
		frm.doc[entry].forEach((item, index) => {
			if (item.name == cdn && item.item_code){
				frappe.call({
					method:"xlevel_petro.xlevel_petro.doctype.sales_entry.sales_entry.get_warehouse_qty",
					args:{
						'item_code':item.item_code,
						'warehouse':warehouse
					},
					callback:function(r){
						if (r.message){
							var available_qty, rate;
							available_qty = r.message.available_qty;
							rate = r.message.rate;
							frappe.model.set_value('Sales Entry Item',item.name,'available_qty',available_qty);
							frappe.model.set_value('Sales Entry Item',item.name,'rate',rate);
							if (!frappe.model.get_value('Sales Entry Item',item.name,'available_qty'))
								frappe.model.set_value('Sales Entry Item',item.name,'available_qty',0);
							if (!frappe.model.get_value('Sales Entry Item',item.name,'rate'))
								frappe.model.set_value('Sales Entry Item',item.name,'rate',0);
						}
					}
				})
			}
		});
	});
};

xlevel_petro.sales_entry.update_amount = function(frm, cdt, cdn) {
	let entry_list = ['sales_item'];
	entry_list.forEach((entry) => {
		frm.doc[entry].forEach((item, index) => {
			if (item.name == cdn && item.item_code && item.qty && item.rate){
				var amount;
				amount = Math.round((item.available_qty - item.qty) * item.rate).toFixed(2);
				frappe.model.set_value('Sales Entry Item',item.name, 'amount',amount)
			}
		});
	});
};